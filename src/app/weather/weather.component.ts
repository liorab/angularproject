import { WeatherRaw } from './../interfaces/weather-raw';
import { WeatherService } from './../services/weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  constructor(private route: ActivatedRoute, private weatherService: WeatherService, private router: Router) { }
  tempData$: Observable<any[]>
  city: string;
  temperature: number;
  image: String;
  errorMessage: String;
  temp_min: number;
  temp_max: number;
  speed: number;
  description: string;
  dt_txt: string;
  hasError: Boolean = false;
  cities: object[] = [{ id: 2, name: 'Amsterdam' }, { id: 3, name: 'Berlin' }, { id: 4, name: 'Bucharest' }, { id: 5, name: 'Budapest' }, { id: 6, name: 'London' }, { id: 7, name: 'Madrid' }, { id: 8, name: 'Paris' }, { id: 9, name: 'Rome' }, { id: 10, name: 'Wien' }];

  ngOnInit() {
    this.city = this.route.snapshot.params.city;
    if (this.city) {
      this.dayliData();
    }
  }
  onSubmit(city: string) {
    this.city = city;
    this.dayliData();
  }
  dayliData() {
    this.weatherService.searchWeatherData(this.city).subscribe(data => {
      this.tempData$ = data.list;
      let j = 0;
      for (let i = 0; i < 40; i++) {
        if (this.tempData$[i].dt_txt.substring(11, 13) == "12") {
          this.tempData$[j] = this.tempData$[i];
          this.tempData$[j].main.temp_max = this.tempData$[i].main.temp_max;
          if (i > 2) {
            this.tempData$[j].main.temp_min = this.tempData$[i - 3].main.temp_min;
          }
          else {
            this.tempData$[j].main.temp_min = this.tempData$[0].main.temp_min;
          }
          j++;
        }
      }

    });
    return this.tempData$;
  }

  imagefinder(icon: string) {
    return `http://api.openweathermap.org/img/w/${icon}.png`
  }

}
