export interface WeatherRaw {

  // "dt":1586736000,
  main: {
    temp: number,
    feels_like: number,
    temp_min: number,
    temp_max: number,
    humidity: number,
  },

  weather: [
    {
      main: string,
      description: string,
      icon: string
    }
  ],
  wind: {
    speed: number
  },

  dt_txt: string,
  day?:string

  // city: {

  //   name: string,
  //   coord: {
  //     lat: number,
  //     lon: number,
  //   }

  // }
}
