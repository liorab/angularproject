export interface Weather {
    name?:string,
    image:string,
    description:string,
    temperature:number,
    temp_min: number,
    temp_max:number,
    speed:number,
    dt_txt:string,
    lat?:number,
    lon?:number ,
    day?:string   
}
