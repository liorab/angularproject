import { AuthService } from '../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authService: AuthService, public router: Router, public route: ActivatedRoute) { }

  email: string;
  password: string;
  confirmPassword: string;
  abc: boolean = false;
  emailError: string;
  passwordError: string;
  confirmPasswordError: string;


  ngOnInit() {
  }

  onSubmit() {
    this.authService.signup(this.email, this.password);
  }

  checkEmail() {
    if (this.email == "") {
      this.emailError = "Email address is required";
    }
    else {
      if (!this.ValidateEmail()) {
        this.emailError = "This email address id badly formatted";
      }
    }
  }

  ValidateEmail() {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
      return (true)
    }
    //alert("You have entered an invalid email address!")
    return (false)
  }

  checkPassword() {
    if (this.password.length == 0) {
      this.passwordError = "Password is required";
    }
    else if (this.password.length > 0 && this.password.length < 8) {
      this.passwordError = "Your password is not strong enough";
    }
    else if (this.password.length >= 8) {
      this.passwordError = "";
    }
  }

  checkConfirmPassword() {
    if (this.confirmPassword.length == 0) {
      this.confirmPasswordError = "Confirm password is required ";
    }
    else if (this.confirmPassword!==this.password){
      this.confirmPasswordError="Passwords do not match";
    }
  }


}
