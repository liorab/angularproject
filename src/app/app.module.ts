import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatMenuModule} from '@angular/material/menu'; 
import { MatSliderModule } from '@angular/material/slider';
import {MatRippleModule} from '@angular/material/core'; 

import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import { FormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { DatePipe } from '@angular/common';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {AngularFireStorageModule} from '@angular/fire/storage';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component'; 
import {MatCheckboxModule} from '@angular/material/checkbox';
import { PredictionComponent } from './prediction/prediction.component';
import { TracksComponent } from './tracks/tracks.component';
import { WeatherComponent } from './weather/weather.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { HistoryComponent } from './history/history.component';
import { HomeComponent } from './home/home.component';
import { RentingComponent } from './renting/renting.component';
import { ConfirmOrderComponent } from './confirm-order/confirm-order.component';

const appRoutes: Routes = [
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: 'prediction', component: PredictionComponent},
  { path: 'tracks', component: TracksComponent},
  { path: 'weather', component: WeatherComponent},
  { path: 'weather/:city', component: WeatherComponent},
  { path: 'suppliers', component: SuppliersComponent},
  { path: 'History', component: HistoryComponent},
  { path: 'Home', component: HomeComponent},
  { path: 'Renting', component: RentingComponent},
  { path: 'Confirm', component: ConfirmOrderComponent},
  { path: "",
    redirectTo: '/Home',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    PredictionComponent,
    TracksComponent,
    WeatherComponent,
    SuppliersComponent,
    HistoryComponent,
    HomeComponent,
    RentingComponent,
    ConfirmOrderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatIconModule,
    MatListModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatSliderModule,
    MatRippleModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MatIconModule,
    MatListModule,
    RouterModule,
    AngularFireAuthModule,
    BrowserModule,
    MatCardModule,
    MatFormFieldModule,
   MatSelectModule,
    MatInputModule,
    MatInputModule,
    MatExpansionModule,
    FormsModule,
    HttpClientModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    AngularFireModule.initializeApp(environment.firebaseConfig),

  ],
  exports: [MatButtonModule, MatCheckboxModule, MatInputModule, MatMenuModule, MatSidenavModule, MatButtonToggleModule, MatExpansionModule, MatRippleModule],
  providers: [DatePipe,AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
