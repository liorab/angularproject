import { Observable } from 'rxjs';
import { TracksService } from './../services/tracks.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.css']
})
export class TracksComponent implements OnInit {

  constructor(public tracksService:TracksService, public router:Router, public route:ActivatedRoute, public authService:AuthService) { }

  Tracks$:Observable<any>;

  ngOnInit() {
    this.Tracks$=this.tracksService.getTracks();
  }

}
