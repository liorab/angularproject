import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { DatePipe } from '@angular/common';
import { pipe } from 'rxjs/internal/util/pipe';


@Injectable({
  providedIn: 'root'
})
export class RentingService {
  pipe = new DatePipe('en-US');
  constructor(private db:AngularFirestore) { }

  rentCollection:AngularFirestoreCollection=this.db.collection('rent');
  userCollection:AngularFirestoreCollection=this.db.collection('users');


  saveRent(userId:string,startDate:Date, endDate:Date, children_bicycles:number, Road_bicycles:number,Mountain_bicycles:number,Electric_bicycles:number,Helmet:number,Glasses:number, Protectors:number, totalAmount:number){
   
    const start = startDate;
    const end = endDate;
    const rent={startDate:start , endDate:end , children_bicycles:children_bicycles , Road_bicycles:Road_bicycles , Mountain_bicycles:Mountain_bicycles , Electric_bicycles:Electric_bicycles , Helmet:Helmet , Glasses:Glasses , Protectors:Protectors,total_Amount:totalAmount}
  //  this.rentCollection=this.db.collection(`users/${userId}/rent`);
    this.db.collection(`users/${userId}/rent`).add(rent);
  }

}
