import { Observable } from 'rxjs';
import { Tracks } from './../interfaces/tracks';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TracksService {

  trackurl="https://www.cyclestreets.net/api/journey.json?key=966a99af07fb9227&itinerarypoints=0.117950,52.205302,City+Centre|0.131402,52.221046,Mulberry+Close|0.147324,52.199650,Thoday+Street&plan=quietest";

  constructor(private http:HttpClient,private db:AngularFirestore,public router:Router) { }

  getTracks(){
    return this.http.get<Tracks[]>(this.trackurl);
  }
}
