import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Weather } from '../interfaces/weather';
import { WeatherRaw } from './../interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient) { }

  private URL = "http://api.openweathermap.org/data/2.5/forecast?q=";
  private KEY = "cb6f6b60144c91456378f3adf6a4bbfa";
  private IMP = "&units=metric";
  private DAYS = 40;

  searchWeatherData(city: string): Observable<any> {  
    return this.http.get<any>(`${this.URL}${city}&cnt=${this.DAYS}&appid=${this.KEY}${this.IMP}`)  
  }
  private handleError(res: HttpErrorResponse) {
    // console.error(res.error);
    return throwError(res.error || 'Server error');
  }
  // private transformWeatherData(data: WeatherRaw): Weather {
  //   return {
  //     image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
  //     description: data.weather[0].description,
  //     temperature: data.main.temp,
  //     temp_min: data.main.temp_min,
  //     temp_max:data.main.temp_max,
  //     speed:data.wind.speed,

  //     dt_txt:data.dt_txt,
  //   }
  // }

}
