import { RentingService } from './../services/renting.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-renting',
  templateUrl: './renting.component.html',
  styleUrls: ['./renting.component.css']
})
export class RentingComponent implements OnInit {

  userId: string;
  startDate: Date;
  endDate: Date;
  dateError: string;
  totalDays: number;
  validDates: boolean = false;
  children_bicycles: number = 0;
  Road_bicycles: number = 0;
  Mountain_bicycles: number = 0;
  Electric_bicycles: number = 0;
  Helmet: number = 0;
  Glasses: number = 0;
  Protectors: number = 0;
  total_amount: number = 0;
  submit: boolean = false;



  constructor(public router: Router, public route: ActivatedRoute, public authService: AuthService, public rentingService: RentingService) { }
  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId)
      })

  }

  onSubmit() {
    if (this.submit) {
      this.rentingService.saveRent(this.userId, this.startDate, this.endDate, this.children_bicycles, this.Road_bicycles, this.Mountain_bicycles, this.Electric_bicycles, this.Helmet, this.Glasses, this.Protectors, this.total_amount);
      // this.router.navigate(['/Confirm']);
    }
  }

  validateStartDate() {
    var now = new Date();
    var start = new Date(this.startDate);
    var end = new Date(this.endDate);
    const utc1 = Date.UTC(now.getFullYear(), now.getMonth(), now.getDate());
    const utc2 = Date.UTC(start.getFullYear(), start.getMonth(), start.getDate());
    const utc3 = Date.UTC(end.getFullYear(), end.getMonth(), end.getDate());
    if (utc2 < utc1) {
      this.dateError = "The rent's start date allready passed";
      this.validDates = false;
      this.totalDays = null;
    }
    else if (utc2 && utc3 && utc3 < utc2) {
      this.dateError = "The rent's end date is before the start date";
      this.validDates = false;
      this.totalDays = null;
    }
    else {
      this.dateError = "";
      this.checkTotalDays();
    }


  }
  checkTotalDays() {
    if (this.startDate && this.endDate) {
      var start = new Date(this.startDate);
      var end = new Date(this.endDate);
      const utc1 = Date.UTC(start.getFullYear(), start.getMonth(), start.getDate());
      const utc2 = Date.UTC(end.getFullYear(), end.getMonth(), end.getDate());
      var check = Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));

      if (check > 0) {
        this.totalDays = check;
        this.validDates = true;
        this.total_amount=(this.children_bicycles*6+this.Road_bicycles*7+this.Mountain_bicycles*14+this.Electric_bicycles*25)*this.totalDays;
      }
      else {
        this.totalDays = null;
        this.validDates = false;
        this.total_amount=0;
      }
      // for updating the resault in the html component
      var app = app.module('daysApp', []);
      app.controller('daysCntl', function () {
      });

    }
  }
  upBycicleChildren() {

    this.children_bicycles++;
    if (this.totalDays) {
      this.total_amount += 6 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });

  }
  downBycicleChildren() {
    if (this.children_bicycles > 0) {
      this.children_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 6 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upRoad_bicycles() {
    this.Road_bicycles++;
    if (this.totalDays) {
      this.total_amount += 7 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downRoad_bicycles() {
    if (this.Road_bicycles > 0) {
      this.Road_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 7 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upElectric_bicycles() {
    this.Electric_bicycles++;
    if (this.totalDays) {
      this.total_amount += 25 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downElectric_bicycles() {
    if (this.Electric_bicycles > 0) {
      this.Electric_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 25 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upMountain_bicycles() {
    this.Mountain_bicycles++;
    if (this.totalDays) {
      this.total_amount += 14 * this.totalDays;
    }
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downMountain_bicycles() {
    if (this.Mountain_bicycles > 0) {
      this.Mountain_bicycles--;
      if (this.totalDays) {
        this.total_amount -= 14 * this.totalDays;
      }
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upHelmet() {
    this.Helmet++;
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downHelmet() {
    if (this.Helmet > 0) {
      this.Helmet--;
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upGlasses() {
    this.Glasses++;
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });

  }
  downGlasses() {
    if (this.Glasses > 0) {
      this.Glasses--;
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }

  upProtectors() {
    this.Protectors++;
    var app = app.module('byChApp', []);
    app.controller('byChCtrl', function ($scope) {
    });
  }
  downProtectors() {
    if (this.Protectors > 0) {
      this.Protectors--;
      var app = app.module('byChApp', []);
      app.controller('byChCtrl', function ($scope) {
      });
    }
  }
  changeSubmit() {
    this.submit = true;
    this.onSubmit();
  }
}
