// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCAoZYkyHEvnQcN50EYaHTHy74Uxa7lUY0",
    authDomain: "angularpr-a42d5.firebaseapp.com",
    databaseURL: "https://angularpr-a42d5.firebaseio.com",
    projectId: "angularpr-a42d5",
    storageBucket: "angularpr-a42d5.appspot.com",
    messagingSenderId: "389294504555",
    appId: "1:389294504555:web:cf7c4c6bea91faf2bccf2e",
    measurementId: "G-8E7XSK7MYK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
